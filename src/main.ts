import { invoke } from "@tauri-apps/api/tauri";

let greetInputEl: HTMLInputElement | null;
let greetMsgEl: HTMLElement | null;

let greetInputEl1: HTMLInputElement | null;
let greetMsgEl1: HTMLElement | null;

async function greet() {
  if (greetMsgEl && greetInputEl) {
    // Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
    greetMsgEl.textContent = await invoke("greet", {
      name: greetInputEl.value,
    });
  }
}

async function greet1() {
  if (greetMsgEl1 && greetInputEl1) {
    // Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
    greetMsgEl1.textContent = await invoke("greet1", {
      name: greetInputEl1.value,
    });
  }
}

window.addEventListener("DOMContentLoaded", () => {
  greetInputEl = document.querySelector("#greet-input");
  greetMsgEl = document.querySelector("#greet-msg");
  document
    .querySelector("#greet-button")
    ?.addEventListener("click", () => greet());
});

window.addEventListener("DOMContentLoaded", () => {
  greetInputEl1 = document.querySelector("#greet-input1");
  greetMsgEl1 = document.querySelector("#greet-msg1");
  document
    .querySelector("#greet-button1")
    ?.addEventListener("click", () => greet1());
});
